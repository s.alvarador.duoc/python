def ejemplo():
    return True

array = [1,2,3,4,5]
array_2 = [1,'dos',3,4,6,ejemplo]

print(array_2[5]())
print(array_2[2])

print(array)
print(array_2)


#para agregar valores usamos append()
array.append(6)
print(array)

#para eliminar un elemento de un lugar en especifico usamos pop()

array.pop(3)
print(array)

#para revertir la lista usamos reverse()

array.reverse()
print(array)

#para obtener una coleccion nueva de la lista según algunas condiciones tenemos lo siguiente:
#array[desde:hasta:saltos]

array_2 = array[-2:5:-2]
print(array_2)
