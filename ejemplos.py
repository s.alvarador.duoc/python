#http://www.pythondiario.com/p/ejercicios-de-programacion-python.html

# Definir una funcion max() que tome como argumento dos números y devuelva el mayor


def max(numero_uno, numero_dos):
    if numero_uno > numero_dos or numero_uno == numero_dos:
        return numero_uno
    else:
        return numero_dos


print(max(1, 2))


# definir una función que tome un carácter y devuelva True si es una vocal, de lo contrario devuevla False


def es_vocal(caracter):
    vocales = ['a', 'e', 'i', 'o', 'u']
    if caracter in vocales:
        return True
    else:
        return False


print(es_vocal('b'))

# definir una funcion sum() que reciba un array y retorne la suma de todos los valores


def sum(numeros):
    suma = 0
    for num in numeros:
        suma += num
    return suma


print(sum([1, 2, 3, 4, 5]))



#dfinir una funcion que nos devuelva un texto invertido

def inversa(texto):
    return texto[::-1]

print(inversa("hola"))




#definir una funcion que multiplique los numeros de una lista enviada como parametro
#definir una funcion que reconoce palíandromos (palabras que tienen el mismo aspecto escritras invertidas como radar)
#definir una función que genere n caracter según un numero entero por ejemplo generr_n_caracteres(5,'x') retorna 'xxxxx'
#definir una función que cuente los digitos de un numero dano
#construir un pequeño programa que transforme binario a entero (101 = 5)
